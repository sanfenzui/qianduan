import request from '@/utils/request'

// 查询replace列表
export function listReplace(query) {
    return request({
        url: '/module/replace/list',
        method: 'get',
        params: query
    })
}

// 查询replace详细
export function getReplace (id) {
    return request({
        url: '/module/replace/edit?id='+id,
        method: 'get'
    })
}


// 新增replace
export function addReplace(data) {
    return request({
        url: '/module/replace/add',
        method: 'post',
        data: data
    })
}

// 修改replace
export function updateReplace(data) {
    return request({
        url: '/module/replace/edit',
        method: 'post',
        data: data
    })
}

// 删除replace
export function delReplace(ids) {
    return request({
        url: '/module/replace/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

