import request from '@/utils/request'

// 查询domain列表
export function listDomain(query) {
    return request({
        url: '/module/domain/list',
        method: 'get',
        params: query
    })
}

// 查询domain详细
export function getDomain (id) {
    return request({
        url: '/module/domain/edit?id='+id,
        method: 'get'
    })
}


// 新增domain
export function addDomain(data) {
    return request({
        url: '/module/domain/add',
        method: 'post',
        data: data
    })
}

// 修改domain
export function updateDomain(data) {
    return request({
        url: '/module/domain/edit',
        method: 'post',
        data: data
    })
}

// 删除domain
export function delDomain(ids) {
    return request({
        url: '/module/domain/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

