import request from '@/utils/request'

// 查询fraudfilter列表
export function listFraudfilter(query) {
    return request({
        url: '/cloakConfig/fraudfilter/list',
        method: 'get',
        params: query
    })
}

// 查询fraudfilter详细
export function getFraudfilter (id) {
    return request({
        url: '/cloakConfig/fraudfilter/edit?id='+id,
        method: 'get'
    })
}


// 新增fraudfilter
export function addFraudfilter(data) {
    return request({
        url: '/cloakConfig/fraudfilter/add',
        method: 'post',
        data: data
    })
}

// 修改fraudfilter
export function updateFraudfilter(data) {
    return request({
        url: '/cloakConfig/fraudfilter/edit',
        method: 'post',
        data: data
    })
}

// 删除fraudfilter
export function delFraudfilter(ids) {
    return request({
        url: '/cloakConfig/fraudfilter/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

