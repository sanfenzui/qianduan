import request from '@/utils/request'

// 查询cloakChoice列表
export function listCloakChoice(query) {
    return request({
        url: '/cloakConfig/cloakChoice/list',
        method: 'get',
        params: query
    })
}

// 原版查询cloakChoice详细
export function getCloakChoice (id) {
    return request({
        url: '/cloakConfig/cloakChoice/edit?id='+id,
        method: 'get'
    })
}
// 自己添加查询cloakChoice详细，并写入到下拉框里
export function getCloakChoiceSelect () {
    return request({
        url: '/cloakConfig/cloakChoice/select',
        method: 'get'
    })
}

// 新增cloakChoice
export function addCloakChoice(data) {
    return request({
        url: '/cloakConfig/cloakChoice/add',
        method: 'post',
        data: data
    })
}

// 修改cloakChoice
export function updateCloakChoice(data) {
    return request({
        url: '/cloakConfig/cloakChoice/edit',
        method: 'post',
        data: data
    })
}

// 删除cloakChoice
export function delCloakChoice(ids) {
    return request({
        url: '/cloakConfig/cloakChoice/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

