import request from '@/utils/request'

// 查询trafficarmor列表
export function listTrafficarmor(query) {
    return request({
        url: '/cloakConfig/trafficarmor/list',
        method: 'get',
        params: query
    })
}

// 查询trafficarmor详细
export function getTrafficarmor (id) {
    return request({
        url: '/cloakConfig/trafficarmor/edit?id='+id,
        method: 'get'
    })
}


// 新增trafficarmor
export function addTrafficarmor(data) {
    return request({
        url: '/cloakConfig/trafficarmor/add',
        method: 'post',
        data: data
    })
}

// 修改trafficarmor
export function updateTrafficarmor(data) {
    return request({
        url: '/cloakConfig/trafficarmor/edit',
        method: 'post',
        data: data
    })
}

// 删除trafficarmor
export function delTrafficarmor(ids) {
    return request({
        url: '/cloakConfig/trafficarmor/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

