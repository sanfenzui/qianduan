import request from '@/utils/request'

// 查询cloakplus列表
export function listCloakplus(query) {
    return request({
        url: '/cloakConfig/cloakplus/list',
        method: 'get',
        params: query
    })
}

// 查询cloakplus详细
export function getCloakplus (id) {
    return request({
        url: '/cloakConfig/cloakplus/edit?id='+id,
        method: 'get'
    })
}


// 新增cloakplus
export function addCloakplus(data) {
    return request({
        url: '/cloakConfig/cloakplus/add',
        method: 'post',
        data: data
    })
}

// 修改cloakplus
export function updateCloakplus(data) {
    return request({
        url: '/cloakConfig/cloakplus/edit',
        method: 'post',
        data: data
    })
}

// 删除cloakplus
export function delCloakplus(ids) {
    return request({
        url: '/cloakConfig/cloakplus/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

