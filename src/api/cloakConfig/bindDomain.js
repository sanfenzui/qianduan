import request from '@/utils/request'

// 获取当前用户所有bindDomain
export function allBindDomain() {
    return request({
        url: '/cloakConfig/bindDomain/all',
        method: 'get'
    })
}
// 获取当前用户所有未使用的bindDomain
export function allNoUsedBindDomain() {
    return request({
        url: '/cloakConfig/bindDomain/noUsedAll',
        method: 'get'
    })
}
// 查询bindDomain列表
export function listBindDomain(query) {
    return request({
        url: '/cloakConfig/bindDomain/list',
        method: 'get',
        params: query
    })
}

// 查询bindDomain详细
export function getBindDomain (id) {
    return request({
        url: '/cloakConfig/bindDomain/edit?id='+id,
        method: 'get'
    })
}


// 新增bindDomain
export function addBindDomain(data) {
    return request({
        url: '/cloakConfig/bindDomain/add',
        method: 'post',
        data: data
    })
}

// 修改bindDomain
export function updateBindDomain(data) {
    return request({
        url: '/cloakConfig/bindDomain/edit',
        method: 'post',
        data: data
    })
}

// 删除bindDomain
export function delBindDomain(ids) {
    return request({
        url: '/cloakConfig/bindDomain/delete',
        method: 'delete',
        data:{ids:ids}
    })
}

